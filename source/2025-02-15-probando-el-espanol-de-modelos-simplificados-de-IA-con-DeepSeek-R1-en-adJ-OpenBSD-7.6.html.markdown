---
title: Probando el español de modelos simplificados de IA con DeepSeek‐R1 en adJ/OpenBSD 7.6
date: 2025-02-15 14:52 UTC
tags: 
---

# Probando el español de modelos simplificados de IA con DeepSeek‐R1 en adJ/OpenBSD 7.6

## 1. Introducción

Se están implementado cada vez más sistemas de inteligencia artificial con
información pública y privada. 

Para mantener la confidencialidad de la información privada consideramos 
necesario que los modelos de IA completen su
entrenamiento y operen en nubes privadas en hardware propio controlado por
quienes protegen la información con altos estándares de seguridad 
incluyendo el sistema operativo.

Aunque en 2024 parecía impensable correr o entrenar esos modelos modernos 
sobre adJ/OpenBSD, gracias a Dios, varios eventos muy recientes han 
permitido realizar este tipo de experimentos en adJ 7.6 que es técnicamente 
fuerte en seguridad (heredada de OpenBSD) y en localización a español:

* La publicación del DeepSeek-V3, Chino en Enero de 2025 como modelo 
  gigante (650 billones de parametros) y de fuentes abiertas 
  (con licencia MIT) que vence en varias métricas a los mejores
  modelos comerciales que son cerrados y costosos; así como su novedosa 
  posibilidad de presentar el "proceso de pensamiento" con DeepSeek-R1
  (ver {deepseek-r1}). 
  Aunado a esto varios usuarios de <https://huggingface.co/> han
  simplificado esos modelos y han combinado Deepseek-R1 con otros modelos
  "pequeños" (e.g 70 billones de parametros o menos).
* Los desarrollos recientes de la herramienta multi-plataforma `llama.cpp` y su
  porte a OpenBSD-current en Enero de 2025. Esta herramienta permite operar 
  con CPUs o con GPUs de tarjetas graficadoras en ocasiones mediante el  porte 
  `vulkan-loader` (que había sido poco usado desde su introducción como se 
  reportó en {openbsd-vulkan})

# 2. Pruebas realizadas

Retroportamos el porte de `llama.cpp` (ver {openbsd-llama-cpp}) a adJ 7.6  y 
en diversos computadores lo compilamos con  vulkan (también pudimos 
compilarlo sin vulkan pero su desempeño con las CPUs que tuvimos a mano 
no permitió usar ni el modelo más  pequeño --por eso mismo el porte
que hicimos para adJ compila con vulkan):

| Designación | CPU | RAM |  Compilación y ejecución de `llama-cli` | GPU según dmesg |
|---|---|---|---|---|
| U620@I5 | Intel Core i5-8265U @1.60GHz | 16G | Bien con inteldrm, ejecución no requiere doas |  Intel(R) UHD Graphics 620 (WHL GT2) |
| Renoir@R5 | AMD Ryzen 5 4600G @3700Mhz | 14G | Bien con amdgpu, ejecución no requiere doas |  AMD Radeon Graphics (RADV RENOIR) (radv) | 
| Cezanne@R5 | AMD Ryzen 5 4600G @3700Mhz | 25G | Bien con amdgpu, ejecución requiere doas | amdgpu0 "ATI Cezanne" rev 0xc9 |
| Cezanne@R7 | AMD Ryzen 7 5700G @3800 Mhz | 64G | Falla compilación con `cmake[16002]: pinsyscalls addr b3056cff749 code 253, pinoff 0xffffffff (pin 330 b30637d8000-b30637e6670 e670) (libcpin 0 0-0 0) error 78`, instalando paquete compilado opera bien con doas | amdgpu0 "ATI Cezanne" rev 0xc8 |


Anotamos que para probar vulkan en OpenBSD/adJ es útil instalar el paquete
`vulkan-tools` y correr `vkcube` con X-Window o `vulkaninfo` desde una 
terminal.

Descargamos diversos modelos de <huggingface.co> en formato GGUF (que es el
soportado por `llama.cpp` aunque el paquete incluye herramientas para 
convertir de otros formatos a GGUF) e iniciamos cada uno con:

<pre>
doas llama-server --host 192.168.1.47 \
  --model bartowski/DeepSeek-R1-Distill-Llama-8B-F16.gguf
</pre>

Cambiando 192.168.1.47 por la IP del computador y
`bartowski/DeepSeek-R1-Distill-Llama-8B-F16.gguf` por el modelo a probar.

Después abrimos la interfaz web que queda en http://192.168.1.47:8080, la
configuramos para presentar la velocidad y temperatura (0 para prevenir
alucinaciones) y realizamos 2 preguntas:

1. ¿Hablas español?
2. ¿Qué conoces de la historia del español?


# 3. Resultados

Escala: Muy Mal, Mal, Regular, Bien, Muy Bien, Excelente

| Modelo .gguf | Tamaño del modelo | Hardware mínimo | RAM usada | Velocidad (t/s) | Dominio del español | Precisión en la historia |
|---|---|---|---|---|---|---|
| unsloth/DeepSeek-R1-Distill-Qwen-1.5B-Q2_K | 752MB | I620@I5 | 1.4G | 20 | Muy mal | Muy mal |
| unsloth/DeepSeek-R1-Distill-Qwen-1.5B-Q2_K_L | 807MB | I620@I5 | 2.5G | 19 | Mal | Muy mal |
| unsloth/DeepSeek-R1-Distill-Qwen-1.5B-Q3_K_M | 924MB | I620@I5 | 3G | 19 | Mal | Muy mal |
| unsloth/DeepSeek-R1-Distill-Qwen-1.5B-Q4_K_M | 1.1GB | I620@I5 | 4G | 20 | Regular | Mal |
| unsloth/DeepSeek-R1-Distill-Qwen-1.5B-Q5_K_M | 1.3GB | Renoir@R5 | 1.3G | 25.9 | Regular | Mal |
| unsloth/DeepSeek-R1-Distill-Qwen-1.5B-Q8_0 | 1.9GB | Renoir@R5 | 2G | 20.7 | Regular | Mal |
| bartowski/DeepSeek-R1-Distill-Qwen-1.5B-f16 | 3.5GB | Renoir@R5 | 4G | 11.7 | Regular | Mal |
| bartowski/DeepSeek-R1-Distill-Qwen-7B-Q2_K | 3GB | Renoir@R5 | 4G | 11.1 | Regular | Regular |
| bartowski/DeepSeek-R1-Distill-Qwen-7B-Q8_0 | 8GB | Renoir@R5 | 8G | 4.8 | Bien | Regular |
| bartowski/DeepSeek-R1-Distill-Qwen-7B-f16 | 11GB |  Cezanne@R5 | 14G | 2  | Bien | Regular |
| bartowski/DeepSeek-R1-Distill-Qwen-7B-f32 | 30.4GB | Bloqueó a Cezane@R7  |  | |  |  |
| bartwoski/DeepSeek-R1-Distill-Llama-8B-Q6_K | 6.6GB | Cezanne@R7 | 8.5GB | 5.4 | Bien | Regular |
| bartwoski/DeepSeek-R1-Distill-Llama-8B-Q8_0 | 8.54GB |Cezanne@R7 | 9.7GB | 4.1 | Bien | Bien |  
| bartwoski/DeepSeek-R1-Distill-Llama-8B-F16 | 16.1GB | Cezanne@R7 | 23GB | 2.3 | Muy Bien | Muy Bien |
| unsloth/DeepSeek-R1-Distill-Llama-70B-Q2_K | 26.3GB | Cezanne@R7 | 55GB | 1.3 | Muy Bien | Muy Bien |
| deepseek-ai/DeepSeek-V3 | >1TB | Granja con NVIDIA H800 y más de 1T en RAM usable gratuitamente  desde https://chat.deepseek.com/  | >1T | >20 | Excelente | Excelente |

## 3.1 Ejemplo de Respuestas:

### 3.1.1 deepseek-ai/DeepSeek-V3:

¿Hablas español?

> ¡Sí, hablo español! 🌟 ¿Prefieres que continuemos la conversación en español?
> Estoy aquí para ayudarte en lo que necesites. ¿En qué puedo asistirte hoy? 😊

¿Qué conoces de la historia del español?

> ¡Claro que sí! La historia del español es fascinante y llena de
> transformaciones. Aquí tienes un resumen conciso y estructurado:

>  ---

>  ### Orígenes: Del latín al castellano medieval
>  1. Raíces latinas: El español es una lengua romance, derivada del latín
>     vulgar (hablado por soldados y colonos romanos) tras la conquista 
>  de la Península Ibérica por Roma (siglo III a.C. – V d.C.).  
>  2. Influencias prerromanas: Palabras de lenguas como el ibérico, céltico o
>     vasco (ej: *perro*, *izquierda*).  
>  3. Invasiones germánicas (siglo V): Los visigodos aportaron términos como
>     *guerra* o *yelmo*.  
>  4. Al-Ándalus (711-1492): La conquista musulmana introdujo miles de palabras
>     árabes, como *aceite*, *azúcar*, *alcalde* o *ojalá* (¡"si Alá quiere"!).  

>  ---

> ### Nacimiento del castellano
> 1. Reconquista y variedades: En el norte, el latín evolucionó a dialectos como
>    el castellano, leonés o aragonés. El castellano se impuso como lengua de
>    poder desde el Reino de Castilla.  
> 2. Primeros textos:  
>   - Glosas Emilianenses (siglo X-XI): Anotaciones en romance junto a un texto
>     en latín (consideradas el "primer balbuceo" del español).  
>   - Cantar de Mio Cid (siglo XII): Primera gran obra épica en castellano
>     medieval.  

> ---

> ### Unificación y expansión
>  1. Alfonso X "el Sabio" (siglo XIII): Estándarizó el castellano para usarlo
>     en documentos oficiales, ciencia y literatura.  
> 2. 1492, un año clave:  
>   - Gramática de Nebrija: Primera gramática de una lengua europea moderna,
>     escrita por Antonio de Nebrija.  
>   - Llegada a América: El español se mezcló con lenguas indígenas (náhuatl,
>     quechua, taíno), dando palabras como *chocolate*, *tomate* o *huracán*.  

> ---

> ### Del Siglo de Oro al español global
> 1. Siglo de Oro (XVI-XVII): Autores como Cervantes, Lope de Vega o Quevedo
>    consolidaron el español como lengua literaria.  
> 2. Influencias posteriores: Préstamos del francés (*hotel*), italiano
>    (*soneto*) e inglés moderno (*software*).  
> 3. Hoy:  
>   - Más de 500 millones de hablantes: Segunda lengua materna más hablada del
>     mundo.  
>   - Variantes dialectales: Español de España, México, Argentina, Caribe, etc.,
>     con diferencias en pronunciación, vocabulario y gramática.  

> ---

> ¿Te interesa algún periodo o aspecto en particular? 😊🌍

### 3.1.2 unsloth/DeepSeek-R1-Distill-Llama-8B-Q8_0  con temperatura 0

¿Hablas español?

> ¡Sí, hablo español! ¿En qué puedo ayudarte? 😊

¿Qué conoces de la historia del español?

> Sé que conozco mucho sobre la historia del español! El español es una lengua
> que pertenece a la familia indo-europea, y tiene su origen en el latín, aunque
> también ha recibido influencias de otras lenguas, como el árabe durante el
> período islámico en la península ibérica.

> El español moderno se desarrolló a lo largo de los siglos, y haido cambios
> significativos a lo largo de la historia. Por ejemplo, durante el período
> medieval, el español medieval fue influenciado por el latín y también por el
> árabe, especialmente en las regiones que fueron conquistadas por los
> musulmanes.

> Además, el español se expandió como lengua internacional gracias a la
> conquista de América por parte de los españoles, lo que llevó a su uso en
> territorios que hoy son países hispanos. El virreinato de América y la
> colonización española tuvieron un impacto significativo en el desarrollo de la
> lengua.

> También es importante mencionar que el español fue declarado lengua oficial de
> several países en América del Sur, Central y Norte, así como en España y otros
> territorios.

> Si tienes una pregunta específica sobre la historia del español, estoy aquí
> para ayudarte!


## 4. Conclusiones

Con una GPU simple es posible emplear adJ/OpenBSD en un portatil o
computador de escritorio para correr un DeepSeek-R1 simplificado.

Para ver operando un modelo se requiere más o menos el doble de RAM
(o VRAM que RAM en la GPU) del tamaño  del modelo.

Para verlo operando rápido se requiere  una GPU con un desempeño
proporcional al tamaño del modelo.

Esperamos que este artículo y el porte de `llama.cpp` incluido en
[adJ 7.6](https://aprendiendo.pasosdejesus.org/AdJ_7.6_-_Aprendiendo_de_Jesus_7.6.html/) 
le facilite experimentar.  Oramos para que  también me sirva y le sirva
para mejorar la relación con Dios, dialogando más tiempo con Él que 
con DeepSeek o con cualquier otra herramienta.


## 5. Bibliografía

* {deepseek-r1} DeepSeek-R1: Incentivizing Reasoning Capability in LLMs via
  Reinforcement Learning. Ene.2024. <https://arxiv.org/abs/2501.12948>
* {openbsd-vulkan} OpenBSD Vulkan Support. 2019. <https://www.phoronix.com/news/OpenBSD-Vulkan-Support>
* {llama-cpp-port} Port of llama.cpp to OenBSD. 2025 <https://marc.info/?l=openbsd-ports&m=173851814111713&w=2>
